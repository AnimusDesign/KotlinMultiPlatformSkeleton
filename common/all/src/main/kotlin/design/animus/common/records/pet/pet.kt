package design.animus.common.records.pet

import kotlinx.serialization.Serializable

@Serializable
enum class PetType {
    DOG,
    CAT,
    FISH,
    LIZARD,
    TIGER,
    PANDA,
    NARWHAL,
    PENGUIN,
    BIRD
}

fun PetTypeFromString(type: String) = PetType.values().firstOrNull { it.name == type }

@Serializable
enum class PetGender {
    MALE,
    FEMALE,
    TRANS,
    QUESTIONING,
    OTHER
}

abstract class PetBase {
    abstract val type: PetType
}

@Serializable
data class Pet(
        override val type: PetType,
        override val gender: PetGender,
        override val age: Int,
        override val name: String,
        override val aggressionLevel: Int
) : PetBaseVersionTwo()

data class PetResponse<D : PetBase>(
        val error: Boolean,
        val data: List<D>
)