package design.animus.common.records.pet

import kotlinx.serialization.Serializable

abstract class PetBaseVersionOne : PetBase() {
    abstract val gender: PetGender
    abstract val age: Int
}

@Serializable
data class PetVersionOne(
        override val type: PetType,
        override val gender: PetGender,
        override val age: Int
) : PetBaseVersionOne()