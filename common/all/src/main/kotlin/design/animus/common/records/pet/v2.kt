package design.animus.common.records.pet

import kotlinx.serialization.Serializable

abstract class PetBaseVersionTwo : PetBaseVersionOne() {
    abstract val name: String
    abstract val aggressionLevel: Int
}

@Serializable
data class PetVersionTwo(
        override val type: PetType,
        override val gender: PetGender,
        override val age: Int,
        override val name: String,
        override val aggressionLevel: Int
) : PetBaseVersionTwo()