package design.animus.kotlinmultiplatform.api

import com.codahale.metrics.jvm.FileDescriptorRatioGauge
import com.codahale.metrics.jvm.GarbageCollectorMetricSet
import com.codahale.metrics.jvm.MemoryUsageGaugeSet
import com.codahale.metrics.jvm.ThreadStatesGaugeSet
import design.animus.kotlinmultiplatform.api.controllers.v1.PetsVersionOne
import design.animus.kotlinmultiplatform.api.controllers.v2.PetsVersionTwo
import design.animus.kotlinmultiplatform.api.graphql.GraphQLEndpoint
import org.jooby.apitool.ApiTool
import org.jooby.json.Jackson
import org.jooby.metrics.Metrics
import org.jooby.run

val Swagger = ApiTool()
val MetricInstance = Metrics()

fun main(args: Array<String>) {
    run(*args) {
        use(Jackson()
                .raw()
        )
        use(Swagger
                .filter { r -> r.pattern().startsWith("/api") }
                .swagger()
                .raml())
        use(MetricInstance
                .request()
                .ping()
                .threadDump()
                .metric("memory", MemoryUsageGaugeSet())
                .metric("threads", ThreadStatesGaugeSet())
                .metric("gc", GarbageCollectorMetricSet())
                .metric("fs", FileDescriptorRatioGauge())
        )
        use(PetsVersionOne::class)
        use(PetsVersionTwo::class)
        use(GraphQLEndpoint::class)
    }
}