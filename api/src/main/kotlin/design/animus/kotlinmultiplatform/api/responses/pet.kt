package design.animus.kotlinmultiplatform.api.responses

import design.animus.common.records.pet.*

fun buildPetResponse(pets: List<Pet>, version: PetVersions = PetVersions.LATEST): PetResponse<PetBase> {
    return PetResponse(
            error = false,
            data = when (version) {
                PetVersions.ONE ->
                    pets.map {
                        PetVersionOne(
                                type = it.type,
                                gender = it.gender,
                                age = it.age
                        )
                    }
                PetVersions.TWO, PetVersions.LATEST ->
                    pets.map {
                        PetVersionTwo(
                                type = it.type,
                                gender = it.gender,
                                age = it.age,
                                name = it.name,
                                aggressionLevel = it.aggressionLevel
                        )
                    }
            }
    )
}