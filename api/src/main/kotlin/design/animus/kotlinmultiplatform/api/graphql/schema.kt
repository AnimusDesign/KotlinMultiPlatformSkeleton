package design.animus.kotlinmultiplatform.api.graphql

import com.github.pgutkowski.kgraphql.KGraphQL
import com.google.gson.Gson
import design.animus.common.records.pet.Pet
import design.animus.common.records.pet.PetGender
import design.animus.common.records.pet.PetType
import design.animus.kotlinmultiplatform.api.pets
import design.animus.kotlinmultiplatform.api.responses.buildPetResponse
import org.jooby.MediaType
import org.jooby.Request
import org.jooby.Response
import org.jooby.mvc.POST
import org.jooby.mvc.Path
import javax.inject.Inject

val schema = KGraphQL.schema {
    query("pets") {
        resolver { ->
            buildPetResponse(pets).data
        }
    }

    query("petsByType") {
        resolver { petType: PetType ->
            buildPetResponse(pets.filter { it.type == petType }).data
        }
    }

    type<Pet>()
    enum<PetGender>()
    enum<PetType>()
}

data class GraphQLRequest(val query: String = "")

@Path("/graphql")
class GraphQLEndpoint @Inject constructor() {
    @POST
    fun postQuery(req: Request, rsp: Response): String {
        val query = req.body().value()
        val gson = Gson()
        val graphQLQuery = gson.fromJson<GraphQLRequest>(query, GraphQLRequest::class.java)
        val queryResult = schema.execute(graphQLQuery.query)
        rsp.type(MediaType.json)
        return queryResult
    }
}