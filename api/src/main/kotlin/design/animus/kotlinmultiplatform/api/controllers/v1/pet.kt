package design.animus.kotlinmultiplatform.api.controllers.v1

import design.animus.common.records.pet.*
import design.animus.kotlinmultiplatform.api.pets
import design.animus.kotlinmultiplatform.api.responses.PetVersions
import design.animus.kotlinmultiplatform.api.responses.buildPetResponse
import org.jooby.Request
import org.jooby.mvc.GET
import org.jooby.mvc.Path
import javax.inject.Inject




@Path("/api/v1/pets")
class PetsVersionOne @Inject constructor() {
    @GET
    fun getAllPets(): PetResponse<PetBase> {
        return buildPetResponse(pets, PetVersions.ONE)
    }

    @GET
    @Path("/type/:petType")
    fun getAllPetsOfSpecificType(req: Request): PetResponse<PetBase> {
        val stringType = req.param("petType").value()
        val petType = PetTypeFromString(stringType)
        return buildPetResponse(pets.filter { it.type == petType }, PetVersions.ONE)
    }
}