package design.animus.kotlinmultiplatform.api.responses

enum class PetVersions(version:String) {
    ONE("one"),
    TWO("two"),
    LATEST("latest")
}