package design.animus.kotlinmultiplatform.api

import design.animus.common.records.pet.Pet
import design.animus.common.records.pet.PetGender
import design.animus.common.records.pet.PetType
import design.animus.common.records.pet.PetVersionOne

val pets = listOf(
        Pet(
                type = PetType.DOG,
                gender = PetGender.MALE,
                age = 6,
                aggressionLevel = 3,
                name = "Joe"
        ),
        Pet(
                type = PetType.PANDA,
                gender = PetGender.OTHER,
                age = 13,
                aggressionLevel = 10,
                name = "Rice Bob"
        )
)